import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { FamilyService } from '../../services/family.service';

@Component({
  selector: 'lab-js-add-family',
  templateUrl: './add-family.component.html',
  styleUrls: ['./add-family.component.scss']
})
export class AddFamilyComponent implements OnInit {
  public familyForm: FormGroup;
  public get children() {
    return this.familyForm.get('children') as FormArray;
  }
  public constructor(
    private readonly familyService: FamilyService
  ) {

  }
  public ngOnInit(): void {
    this.initForm();
  }
  public addChild() {
    const newChild = this.createNewFamily();
    (this.familyForm.get('children') as FormArray).push(newChild);
  }
  public removeChild(index: number) {
    (this.familyForm.get('children') as FormArray).removeAt(index);
  }
  public createNewFamily() {
    return new FormGroup({
      name: new FormControl(null, [Validators.required]),
      age: new FormControl(null, [Validators.required])
    });
  }
  public submit() {
    console.log(this.familyService.addFamily$({ ...this.familyForm.value }).subscribe());
  }
  public initForm() {
    return this.familyForm = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      father: this.createNewFamily(),
      mother: this.createNewFamily(),
      children: new FormArray([this.createNewFamily()])
    })
  }
}
